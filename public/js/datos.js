var html = {
	'nombre':'HTML',
	'enlace':'',
	'icono':''};
var js = {
	'nombre':'JavaScript',
	'enlace':'',
	'icono':''};
var css = {
	'nombre':'CSS',
	'enlace':'',
	'icono':''};
var latex = {
	'nombre':'LaTeX',
	'enlace':'',
	'icono':''};
var fontawesome = {
	'nombre':'Fontawesome',
	'enlace':'',
	'icono':''};
var bootstrap = {
	'nombre':'Bootstrap',
	'enlace':'',
	'icono':''};
var jquery = {
	'nombre':'JQuery',
	'enlace':'',
	'icono':''};
var hammer = {
	'nombre':'Hammer.js',
	'enlace':'https://hammerjs.github.io/',
	'icono':''};
var cordova = {
	'nombre':'Apache Cordova',
	'enlace':'https://cordova.apache.org/',
	'icono':''};
var cocoon = {
	'nombre':'Cocoon by Ludei',
	'enlace':'https://blog.cocoon.io/',
	'icono':''};

//tags
var grado = {
	'badge':'btn btn-primary btn-sm',
	'nombre':'grado'
}
var personal = {
	'badge':'btn btn-dark btn-sm',
	'nombre':'personal'
}
var curso = {
	'badge':'btn btn-info btn-sm',
	'nombre':'curso'
}

var proyectos = [{
	'titulo':'Tetris',
	'descripcion':'Proyecto de la asignatura de Diseño de Aplicaciones Web Enriquecidas.',
	'fotos':[{'src':'imagenes/proyectos/full/tetris.png',
            'thumb':'imagenes/proyectos/thumbnails/tetris.jpg',
            'subHtml':'<h3>Tetris</h3><p>Aspecto final de la aplicación</p>'
            },
			{'src':'imagenes/proyectos/full/tetris2.png',
            'thumb':'imagenes/proyectos/thumbnails/tetris2.jpg',
            'subHtml':'<h3>Plataformas</h3><p>Sistemas operativos y plataformas utilizadas en el desarrollo</p>'
            }
            ],
	'anio':'2015',
	'curso':'4',
	'tecnologias':[html, js, css, hammer, cordova, cocoon],
	'archivos':[{'icono':'far fa-file-pdf', 
				'nombre':'informe', 
				'enlace':'https://gitlab.com/david-ramirez/tetris/uploads/651a4fa63508452248613e167127304c/DAWE_Tetris_D_Ramirez.pdf'},
				{'icono':'fab fa-android', 
				'nombre':'Apk debug Cordova', 
				'enlace':'https://gitlab.com/david-ramirez/tetris/uploads/ce741798f234a283c3b320de2756abe8/tetris-debug-cordova.apk'},
				{'icono':'fab fa-android', 
				'nombre':'Apk debug Cocoon Ludei', 
				'enlace':'https://gitlab.com/david-ramirez/tetris/uploads/d87753005245187f7794ffa4340b99f4/tetris-debug-ludei.apk'}
				],
	'enlaceRepo':{'icono':'fab fa-gitlab', 
					'nombre':'tetris', 
					'enlace':'https://gitlab.com/david-ramirez/tetris'},
	'enlacePrueba':{'icono':'fab fa-gitlab', 
					'nombre':'tetris', 
					'enlace':'https://david-ramirez.gitlab.io/tetris/'},
	'tags':[grado]
},{
	'titulo':'CV',
	'descripcion':'Curriculum creado en LaTeX a partir de la plantilla de <a href="https://www.latextemplates.com/template/twenty-seconds-resumecv">latextemplates.com</a>, basada a su vez en el trabajo de <a href="https://github.com/spagnuolocarmine/TwentySecondsCurriculumVitae-LaTex">Carmine Spagnuolo</a>.',
	'fotos':[{'src':'imagenes/proyectos/full/cv.png',
			'thumb':'imagenes/proyectos/thumbnails/cv.jpg'}],
	'nogallery':'true',
	'anio':'2019',
	'tecnologias':[latex, fontawesome],
	'archivos':[{'icono':'far fa-file-pdf', 
				'nombre':'', 
				'enlace':''},
				{'icono':'flag-icon flag-icon-es', 
				 'nombre':'', 
				 'enlace':'https://gitlab.com/david-ramirez/cv-latex/raw/master/CV_D_Ramirez_ES.pdf'},
				 {'icono':'flag-icon flag-icon-gb', 
				 'nombre':'', 
				 'enlace':'https://gitlab.com/david-ramirez/cv-latex/raw/master/CV_D_Ramirez_EN.pdf'}/*,
				 {'icono':'flag-icon flag-icon-fr', 
				 'nombre':'', 
				 'enlace':'#'},
				 {'icono':'flag-icon flag-icon-de', 
				 'nombre':'', 
				 'enlace':'#'}*/
				 ],
	'enlaceRepo':{'icono':'fab fa-gitlab', 
				  'nombre':'cv', 
				  'enlace':'https://gitlab.com/david-ramirez/cv-latex'},
	'tags':[personal]
},{
	'titulo':'Página de presentación',
	'descripcion':'Página personal de presentación de proyectos.',
	'fotos':[{'src':'imagenes/proyectos/full/pagpersonal.png',
			  'thumb':'imagenes/proyectos/thumbnails/pagpersonal.jpg'}],
	'nogallery':'true',
	'anio':'2019',
	'tecnologias':[html, js, css, fontawesome, bootstrap, jquery],
	'enlaceRepo':{'icono':'fab fa-gitlab',
				  'nombre':'presentación', 
				  'enlace':'https://gitlab.com/david-ramirez/david-ramirez.gitlab.io'},
	'tags':[personal]
},{
	'titulo':'WebSMA',
	'descripcion':'Proyecto de la asignatura de Software para Matemática Aplicada.',
	'fotos':[{'src':'imagenes/proyectos/full/websmaindex.png',
            'thumb':'imagenes/proyectos/thumbnails/websmaindex.png',
            'subHtml':'<h3>Índice</h3><p>Índice de la web. Listado de tests disponibles.</p>'
            },
			{'src':'imagenes/proyectos/full/websmatema.png',
            'thumb':'imagenes/proyectos/thumbnails/websmatema.png',
            'subHtml':'<h3></h3><p></p>'
            },
			{'src':'imagenes/proyectos/full/websmatemapausa.png',
            'thumb':'imagenes/proyectos/thumbnails/websmatemapausa.png',
            'subHtml':'<h3></h3><p></p>'
			},
			{'src':'imagenes/proyectos/full/websmatestfin.png',
            'thumb':'imagenes/proyectos/thumbnails/websmatestfin.png',
            'subHtml':'<h3></h3><p></p>'
            }],
	'anio':'2015',
	'curso':'4',
	'tecnologias':[html, js, css],
	'archivos':[{'icono':'far fa-file-pdf', 
				'nombre':'informe', 
				'enlace':'https://gitlab.com/david-ramirez/websma/raw/master/informe.pdf'}
				],
	'enlaceRepo':{'icono':'fab fa-gitlab', 
					'nombre':'WebSMA', 
					'enlace':'https://gitlab.com/david-ramirez/websma'},
	'enlacePrueba':{'icono':'fab fa-gitlab', 
					'nombre':'WebSMA', 
					'enlace':'https://david-ramirez.gitlab.io/websma/'},
	'tags':[grado]
}
];