function animarIconoEliminarBusqueda(){
    ($("#entradaBusqueda").val())? $("#borrarBusqueda").show(500):$("#borrarBusqueda").hide(500);
}

function inicializarGalerias(){
    $('.galeria').each(function(index, el) {
        $(this).on('click', function() {
            proyIndex = $(this).attr('id').slice(7);
            $(this).lightGallery({
                galleryId: proyIndex,
                dynamic: true,
                dynamicEl: proyectos[proyIndex].fotos
            });
        });
    }); 
}

function filtrarCartas(texto){
    animarIconoEliminarBusqueda();
    cambiarCartas(function(index) {
        return $(this).text().toLowerCase().indexOf(texto)>-1;
    });
}

function filtrarCartasEtiqueta(texto){
    animarIconoEliminarBusqueda();
    cambiarCartas(function(index) {
        return $(this).find(".card-footer").text().toLowerCase().indexOf(texto)>-1;
    });
}

function cambiarCartas(filterFunc){
    $("#proyectosCartas").isotope({ 
        itemSelector: '.card',
        percentPosition: true,
        masonry:{columnWidth: '.card'},
        filter: filterFunc});
}

function buscarEtiqueta(tag){
    $("#entradaBusqueda").val(tag.nombre);
    filtrarCartasEtiqueta(tag.nombre);

    //Volver al principio de las cartas
    $("html, body").animate({ scrollTop: $("#proyectos").offset().top }, 500);

}

function mostrarTodasLasCartas(){
    cambiarCartas('*');
}

function cargarCartas(proyectos){
    var html = '';

    for(i in proyectos){
        html += crearCarta(proyectos[i], i);
    }
    $('#proyectosCartas').html(html);

    inicializarGalerias();
}

function borrarBusqueda(){
    $("#entradaBusqueda").val("");
    animarIconoEliminarBusqueda();
    mostrarTodasLasCartas();
}

function crearCarta(p, indice){
    var carta = "";

    carta += '<div id="'+ indice +'" class="card border-primary shadow">';
    carta += '<div class="card-header"><h3 class="card-title text-center font-weight-bold my-0">' + p.titulo + '</h3></div>';

    //galerias / foto
    carta += '<div class="" style="position: relative;height: 18em; width: 100%">';


    if(p.nogallery){
        carta += '<img class="card-img-top mx-auto d-block"  style="height: 18em; width: auto" src="'+ p.fotos[0].thumb + '" class="card-img-top"/>';
    }else{
        fotos = p.fotos;
        if(fotos.length > 1){//icono una/varias img cundo hay galeria
            carta += '<i class="far fa-images h2 text-primary mb-3 mr-4" style="z-index:1;position:absolute;bottom:0px;right:0px;"></i>';
        }else{
            carta += '<i class="far fa-image h2 text-primary mb-3 mr-4" style="z-index:1;position:absolute;bottom:0px;right:0px;"></i>';
        }

        carta += '<div class="galeria" id="galeria'+indice+'" style="z-index:0;position:absolute;left: 0;right: 0;margin: auto">';
        carta += '<img class="card-img-top mx-auto d-block" style="height: 18em; width: auto" src="'+fotos[0].thumb+'" />';
        carta += '</div>';
    }

    carta += '</div>';

    //

    carta += '<ul class="list-group list-group-flush">';
    carta += '<li class="list-group-item"><p class="card-text text-justify">'+ p.descripcion +'</p></li>';
    carta += '<li class="list-group-item"><div class="row justify-content-between px-3"><span><strong>Año: </strong>'+ p.anio +'</span>';
    if(p.curso){
        carta += '<span><strong>Curso: </strong>'+ p.curso +'</span>';
    }
    carta += '</div></li>';

    //tecnologias
    carta += '<li class="list-group-item">';
    carta += '<p class="card-text text-justify">';
    for(t in p.tecnologias){
        carta += (p.tecnologias[t].enlace) ? '<a href="'+ p.tecnologias[t].enlace +'">'+ p.tecnologias[t].nombre +'</a>': '<span>'+p.tecnologias[t].nombre+'</span>';

        if(t != p.tecnologias.length - 1){
            carta += ', ';
        }
    }
    carta += '</p>';
    carta += '</li>';
    carta += '</ul>';

    //archivos
    if(p.archivos){
        carta += '<ul class="list-group list-group-flush">';
        if(p.archivos[0].nombre){//si los archivos tienen nombre, los muestro en líneas separadas
            for(a of p.archivos){
                carta += '<li class="list-group-item"><a href="'+ a.enlace +'">';

                (a.icono)? carta += '<i class="h3 mx-2 my-0 align-middle '+a.icono+'"> </i>':'';
                (a.nombre)? carta += a.nombre:'';

                carta += '</a></li>';
            }    
        }else{
            carta += '<li class="list-group-item text-center">';
            for(a of p.archivos){//Si no lo tienen, muestro los iconos en una línea
                carta += '<a href="'+ a.enlace +'">';
                carta += '<i class="h3 mx-2 '+a.icono+'"> </i>';
                carta += '</a>';
            }
            carta += '</li>'
        }
        
        carta += '</ul>';
    }

    //repositorio
    if(p.enlaceRepo){
        e = p.enlaceRepo;
        carta += '<div class="card-body mx-0 pt-2 '+ ((p.enlacePrueba)? 'pb-0':'pb-2') +' row justify-content-between align-items-center">';//<div class="row justify-content-between">';
        carta += '<span class="align-middle"><strong>Repositorio: </strong></span><a class="" href="'+ e.enlace +'">';

            (e.icono)? carta += '<i class="h3 mx-2 my-0 align-middle '+e.icono+'"> </i>':'';
            (e.nombre)? carta += e.nombre:'';

            carta += '</a>';
        carta += '</div>';//</div>';
    }

    //enlace de prueba
    if(p.enlacePrueba){
        e = p.enlacePrueba;
        carta += '<div class="card-body pt-1 pb-2">';
        carta += '<a class="btn btn-primary btn-block" target="_blank" rel="noopener noreferrer" href="'+ e.enlace +'"><strong>Pruebame: </strong>';

            (e.icono)? carta += '<i class="h3 mx-2 my-0 align-middle '+e.icono+'"> </i>':'';
            (e.nombre)? carta += '<span>'+e.nombre+'</span>':'';

            carta += '</a>';
        carta += '</div>';
    }

    //tags
    carta += '<div class="card-footer"><div class="row justify-content-between px-3"><span><i class="fas fa-tags align-bottom"> </i></span>';
    for(t of p.tags){
        carta +='<button onclick="buscarEtiqueta('+t.nombre+')" class="'+ t.badge + '">'+ t.nombre +'</button>';
    }
    carta += '</div></div>';

    //fin carta
    carta += '</div>';

    return carta;
}